-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 29. led 2020, 15:41
-- Verze serveru: 10.4.11-MariaDB
-- Verze PHP: 7.4.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `veselka`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `clanky`
--

CREATE TABLE `clanky` (
  `id` int(10) NOT NULL,
  `nadpis` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `text` text COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `clanky`
--

INSERT INTO `clanky` (`id`, `nadpis`, `text`) VALUES
(18, '14. Autorský koncert', '14.ročník Autorských koncertů v Lošticích se opět vydařil a hudební skladatelé Jiří Helán, Jiří Tesařík a Josef Žid byli spokojeni i s Loštickou Veselkou a Skalankou ze Švihova, které jim zahrály jejich skladby. Sám pan král Tvarůžek I. se svoji družinou a řezbářem Jaroslavem Benešem rozdávali hostům malé pozornosti, Loštické tvarůžky a dřevěné houslové klíče. Zábavu skvěle moderovali misionáři DH, kapelník Loštické Veselky Stanislav Veselý s dcerou Katkou.'),
(19, '2. Festival dechových hudeb v Čáslavi', 'Přijali jsme pozvání dechové hudby Věnovanky, která nás pozvala na 2. Festival dechových hudeb do Dusíkova divadla v Čáslavi, který se konal v neděli dne 7. 4. 2019. Vystoupily zde s Věnovankou také další soubory, havlíčkobrodská Rebelka a Moravská dechová hudba a naše Loštická Veselka.');

-- --------------------------------------------------------

--
-- Struktura tabulky `galerie`
--

CREATE TABLE `galerie` (
  `id` int(11) NOT NULL,
  `id_clanku` int(11) NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `urlT` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `zobrazit_v_galerii` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `galerie`
--

INSERT INTO `galerie` (`id`, `id_clanku`, `url`, `urlT`, `zobrazit_v_galerii`) VALUES
(117, 18, 'galerie/aktuality/clanek18/1.jpg', 'galerie/aktuality/clanek18/t1.jpg', 1),
(118, 18, 'galerie/aktuality/clanek18/2.jpg', 'galerie/aktuality/clanek18/t2.jpg', 1),
(119, 18, 'galerie/aktuality/clanek18/3.jpg', 'galerie/aktuality/clanek18/t3.jpg', 1),
(120, 18, 'galerie/aktuality/clanek18/4.jpg', 'galerie/aktuality/clanek18/t4.jpg', 1),
(121, 18, 'galerie/aktuality/clanek18/5.jpg', 'galerie/aktuality/clanek18/t5.jpg', 1),
(122, 18, 'galerie/aktuality/clanek18/6.jpg', 'galerie/aktuality/clanek18/t6.jpg', 1),
(123, 18, 'galerie/aktuality/clanek18/7.jpg', 'galerie/aktuality/clanek18/t7.jpg', 1),
(124, 18, 'galerie/aktuality/clanek18/8.jpg', 'galerie/aktuality/clanek18/t8.jpg', 1),
(125, 18, 'galerie/aktuality/clanek18/9.jpg', 'galerie/aktuality/clanek18/t9.jpg', 1),
(126, 18, 'galerie/aktuality/clanek18/10.jpg', 'galerie/aktuality/clanek18/t10.jpg', 1),
(127, 18, 'galerie/aktuality/clanek18/11.jpg', 'galerie/aktuality/clanek18/t11.jpg', 1),
(128, 18, 'galerie/aktuality/clanek18/12.jpg', 'galerie/aktuality/clanek18/t12.jpg', 0),
(129, 18, 'galerie/aktuality/clanek18/13.jpg', 'galerie/aktuality/clanek18/t13.jpg', 0),
(130, 18, 'galerie/aktuality/clanek18/14.jpg', 'galerie/aktuality/clanek18/t14.jpg', 0),
(131, 18, 'galerie/aktuality/clanek18/15.jpg', 'galerie/aktuality/clanek18/t15.jpg', 0),
(132, 18, 'galerie/aktuality/clanek18/16.jpg', 'galerie/aktuality/clanek18/t16.jpg', 0),
(133, 18, 'galerie/aktuality/clanek18/17.jpg', 'galerie/aktuality/clanek18/t17.jpg', 0),
(134, 19, 'galerie/aktuality/clanek19/1.jpg', 'galerie/aktuality/clanek19/t1.jpg', 0),
(135, 19, 'galerie/aktuality/clanek19/2.jpg', 'galerie/aktuality/clanek19/t2.jpg', 1),
(136, 19, 'galerie/aktuality/clanek19/3.jpg', 'galerie/aktuality/clanek19/t3.jpg', 1),
(137, 19, 'galerie/aktuality/clanek19/4.jpg', 'galerie/aktuality/clanek19/t4.jpg', 1),
(138, 19, 'galerie/aktuality/clanek19/5.jpg', 'galerie/aktuality/clanek19/t5.jpg', 1),
(139, 19, 'galerie/aktuality/clanek19/6.jpg', 'galerie/aktuality/clanek19/t6.jpg', 1),
(140, 19, 'galerie/aktuality/clanek19/7.jpg', 'galerie/aktuality/clanek19/t7.jpg', 1),
(141, 19, 'galerie/aktuality/clanek19/8.jpg', 'galerie/aktuality/clanek19/t8.jpg', 1);

-- --------------------------------------------------------

--
-- Struktura tabulky `planovane_akce`
--

CREATE TABLE `planovane_akce` (
  `id` int(11) NOT NULL,
  `nazev` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `misto` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL,
  `datum` date NOT NULL,
  `typ` varchar(255) COLLATE utf8mb4_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_czech_ci;

--
-- Vypisuji data pro tabulku `planovane_akce`
--

INSERT INTO `planovane_akce` (`id`, `nazev`, `misto`, `datum`, `typ`) VALUES
(1, 'Masopust', 'Bouzov', '2020-02-29', 'průvod'),
(2, 'Oslavy 125ti let Sboru dobrovolných hasičů ', 'Líšnice', '2020-06-20', 'koncert'),
(3, 'Festival dechovek Pod Loštickým nebem', 'Loštice', '2020-06-28', 'koncert'),
(4, 'Koncert pro příznivce dechové hudby', 'Mohelnice', '2020-07-28', 'koncert');

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `clanky`
--
ALTER TABLE `clanky`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `galerie`
--
ALTER TABLE `galerie`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_clanku` (`id_clanku`);

--
-- Klíče pro tabulku `planovane_akce`
--
ALTER TABLE `planovane_akce`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `clanky`
--
ALTER TABLE `clanky`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT pro tabulku `galerie`
--
ALTER TABLE `galerie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT pro tabulku `planovane_akce`
--
ALTER TABLE `planovane_akce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `galerie`
--
ALTER TABLE `galerie`
  ADD CONSTRAINT `galerie_ibfk_1` FOREIGN KEY (`id_clanku`) REFERENCES `clanky` (`id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
