<?php include('header.html'); ?>
<title>Úvod</title>

<body>
    <div class="container">
        <div class="box-shadow">
            <div class="row">
					<?php include('head-image.php'); ?>
            </div>
            <div class="row" id="menu">
					<?php include('menu.php'); ?>
            </div>
            <div class="row">
                <div class="col-sm-8 col-lg-8">
                    <div class="content">
                        <h1 class="display-4">O kapele</h1>
                        <p>Loštická Veselka hraje k tanci i k poslechu při různorodých příležitostech jako např. festivaly dechových hudeb, výročí hasičských sborů, obcí, hudební a taneční odpoledne pro příznivce dechové hudby, Vánoční a jiné koncerty.</p>
<p>Každoročně je tato dechová kapela pořadatelem festivalu dechových hudeb v Lošticích s názvem Pod Loštickým nebem,který se koná v druhé polovině měsíce června. Pro tento festival je také typický tvarůžkový stánek s těmi nejvoňavějšími pochutinami, připravených z místního tradičního sýra, tvarůžků.</p>
<p>Na podzim kapela každý rok hostí nové autory při příležitosti Autorského koncertu. Tento koncert je věnován vždy dvěma autorům, jejichž skladeb se ujme domácí kapela Loštická Veselka a hostující kapela, vybraná pro daný ročník. Každá tato kapela divákům představí několik skladeb z autorovy dílny a moderátoři seznámí publikum s jeho životem.</p>
<p>Loštická Veselka má za sebou již 14. ročník a za tu dobu na svém domácím jevišti přivítala již desítky autorů. Mezi nimi například Jiří Helán, Jiří Tesařík, Josef Žid, Jan Slabák, Milan Baginský, Josef Vejvoda, Miloslav Procházka, Miloň Čepelka a další.</p>
<p>Tradicí se taktéž stalo takzvané přátelské koledování. Při těchto vánočních pochůzkách kapela navštěvuje své příznivce a bývalé členy, kterým za zvuku koled zpříjemňuje Vánoční čas.</p>
                        <a data-fancybox="gallery" href="./galerie/uvod/1.jpg"><img class="thumb" src="./galerie/uvod/1_tn.jpg"></a>
                        <a data-fancybox="gallery" href="./galerie/uvod/2.jpg"><img class="thumb" src="./galerie/uvod/2_tn.jpg"></a>
                        <a data-fancybox="gallery" href="./galerie/uvod/3.jpg"><img class="thumb" src="./galerie/uvod/3_tn.jpg"></a>
                        <a data-fancybox="gallery" href="./galerie/uvod/4.jpg"><img class="thumb" src="./galerie/uvod/4_tn.jpg"></a>
                        <a data-fancybox="gallery" href="./galerie/uvod/5.jpg"><img class="thumb" src="./galerie/uvod/5_tn.jpg"></a>
								<a data-fancybox="gallery" href="./galerie/uvod/6.jpg"><img class="thumb" src="./galerie/uvod/6_tn.jpg"></a>
                    </div>
                </div>
                <div class="col-sm-8 col-lg-4">
                    <div class="right">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FLo%25C5%25A1tick%25C3%25A1-Veselka-111782970262375%2F&tabs=timeline%2Cevents&width=340&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1606107826281502" id="facebook" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
								<iframe src="https://api.mapy.cz/frame?params=%7B%22x%22%3A16.92342947434821%2C%22y%22%3A49.769538865610365%2C%22base%22%3A%221%22%2C%22layers%22%3A%5B%5D%2C%22zoom%22%3A11%2C%22url%22%3A%22https%3A%2F%2Fen.mapy.cz%2Fs%2Fmonujozero%22%2C%22mark%22%3A%7B%22x%22%3A%2216.92892263841069%22%2C%22y%22%3A%2249.74469765746639%22%2C%22title%22%3A%22Lo%C5%A1tice%22%7D%2C%22overview%22%3Atrue%7D&amp;width=300&amp;height=270&amp;lang=en" id="mapka" style="border:none" frameBorder="0"></iframe>
                    </div>
                </div>
				</div>
            <div class="row" id="footer">
					<?php include('footer.html'); ?>			
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>
