<div class="col-lg" style="padding-top:5px">
	<nav>
		<ul class="nav nav-tabs">
			<li class="nav-item">
				<a class="nav-link <?php if($_SERVER['SCRIPT_NAME']=="/index.php") { ?>active<?php } ?>"
					href="./index.php">Domů</a>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php if($_SERVER['SCRIPT_NAME']=="/aktuality.php") { ?>active<?php } ?>"
					href="./aktuality.php">Aktuality</a>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php if($_SERVER['SCRIPT_NAME']=="/planovane-akce.php") { ?>active<?php } ?>"
					href="./planovane-akce.php">Plánované akce</a>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php if($_SERVER['SCRIPT_NAME']=="/obsazeni.php") { ?>active<?php } ?>"
					href="./obsazeni.php">Obsazení</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
					aria-haspopup="true" aria-expanded="false">
					Galerie
				</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
					<a class="dropdown-item <?php if($_SERVER['SCRIPT_NAME']=="/foto-galerie.php") { ?>active<?php } ?>"
						href="./foto-galerie.php">Fotografie</a>
					<a class="dropdown-item <?php if($_SERVER['SCRIPT_NAME']=="/video-galerie.php") { ?>active<?php } ?>"
						href="./video-galerie.php">Videa</a>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php if($_SERVER['SCRIPT_NAME']=="/kontakty.php") { ?>active<?php } ?>"
					href="./kontakty.php">Kontakty</a>
			</li>
		</ul>
	</nav>
</div>