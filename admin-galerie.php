<?php 
	if(isset($_POST['submit'])){
		if($_POST['password'] != 'Heslo123'){
			return;
		}
		echo $_POST["nazev"];
		echo $_POST["text_clanku"];
		
		include('db.php');
		include('thumbnail.php');

		$countfiles = count($_FILES['fotky']['name']);
		
		$sql = "INSERT INTO clanky (nadpis, text)
			VALUES ('" . $_POST["nazev"] . "', '" . $_POST["text_clanku"] ."')";

			if ($conn->query($sql) === TRUE) {
				echo "New record created successfully";
				
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
			$id_clanku = $conn->insert_id;

			mkdir('galerie/aktuality/clanek'. $id_clanku , 0777, true);
		// Looping all files
		for($i=0;$i<$countfiles;$i++){
			$filename = $_FILES['fotky']['name'][$i];
			
			// Upload file
			move_uploaded_file($_FILES['fotky']['tmp_name'][$i], 'galerie/aktuality/clanek' . $id_clanku .'/'. $filename);

			createThumbnail('galerie/aktuality/clanek' . $id_clanku .'/'. $filename, 'galerie/aktuality/clanek' . $id_clanku .'/t'. $filename, 300);

			$sql = "INSERT INTO galerie (id_clanku, url, urlT)
			VALUES ($id_clanku, '" . 'galerie/aktuality/clanek' . $id_clanku . "/" . $filename . "', '" . 'galerie/aktuality/clanek' . $id_clanku . "/t" . $filename . "')";

			if ($conn->query($sql) === TRUE) {
				echo "New record created successfully";
			} else {
				echo "Error: " . $sql . "<br>" . $conn->error;
			}

			
		}
		$conn->close();

	}
?>
<?php include('header.html'); ?>
<title>Úvod</title>

<body>
	<div class="container">
		<div class="box-shadow">
			<div class="row">
				<?php include('head-image.php'); ?>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="content">
						<form action="admin-galerie.php" method="POST" enctype='multipart/form-data'>
							<div class="form-group">
								<label for="nazev">Nadpis článku</label>
								<input type="text" class="form-control" id="nazev" name="nazev" placeholder="Nadpis článku">
							</div>
							<div class="form-group">
								<label for="text_clanku">Text článku</label>
								<textarea class="form-control" id="text_clanku" name="text_clanku" rows="5"></textarea>
							</div>
							<div class="form-group">
								<label for="fotky">Fotky</label>
								<input type="file" class="form-control-file" id="fotky" name="fotky[]" accept="image/*"
									multiple="multiple">
							</div>
							<div class="form-group">
								<input type="password" id="pass" name="password" minlength="5" required>
							</div>
							<button type="submit" name="submit" class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
			</div>
			<div class="row" id="footer">
				<?php include('footer.html'); ?>
			</div>
		</div>
	</div>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<script>
	function toggleChevron(e) {
		console.log('AAA');
		$(e.target)
			.prev('.card-header')
			.find("i.fa")
			.toggleClass('fa-chevron-left fa-chevron-down');
	}

	$('#accordion').on('hidden.bs.collapse', toggleChevron);
	$('#accordion').on('shown.bs.collapse', toggleChevron);
	</script>
</body>

</html>