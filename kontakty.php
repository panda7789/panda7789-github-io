<?php include('header.html'); ?>
<title>Kontakty</title>

<body>
	<div class="container">
		<div class="box-shadow">
			<div class="row">
				<?php include('head-image.php'); ?>
			</div>
			<div class="row" id="menu">
				<?php include('menu.php'); ?>
			</div>
			<div class="row">
				<div class="col-sm-8">
					<div class="content">
						<h1 class="display-4">Kontakty</h1>
						<div class="card mb-3" style="max-width: 540px;">
							<div class="row no-gutters">
								<div class="col-md-8">
									<div class="card-body">
										<h5 class="card-title">Markéta Tichá - manager</h5>
										<p class="card-text">Email: losticka.veselka@seznam.cz<br>
											Mobil: 720 974 169</p>
									</div>
								</div>
							</div>
						</div>
						<div class="card mb-3" style="max-width: 540px;">
							<div class="row no-gutters">
								<!--
								<div class="col-md-4">
									<img src="..." class="card-img" alt="...">
								</div>
								-->
								<div class="col-md-8">
									<div class="card-body">
										<h5 class="card-title">Stanislav Veselý</h5>
										<p class="card-text">
										Loštice, Palackého 274<br>
										789 83<br>
										<br>
										Email: piskalek@volny.cz <br>
										Telefon: 583 445 532 <br>
										Mobil: 602 544 253
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
				<div class="right">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FLo%25C5%25A1tick%25C3%25A1-Veselka-111782970262375%2F&tabs=timeline%2Cevents&width=350&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1606107826281502" id="facebook" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
								<iframe src="https://api.mapy.cz/frame?params=%7B%22x%22%3A16.92342947434821%2C%22y%22%3A49.769538865610365%2C%22base%22%3A%221%22%2C%22layers%22%3A%5B%5D%2C%22zoom%22%3A11%2C%22url%22%3A%22https%3A%2F%2Fen.mapy.cz%2Fs%2Fmonujozero%22%2C%22mark%22%3A%7B%22x%22%3A%2216.92892263841069%22%2C%22y%22%3A%2249.74469765746639%22%2C%22title%22%3A%22Lo%C5%A1tice%22%7D%2C%22overview%22%3Atrue%7D&amp;width=300&amp;height=270&amp;lang=en" id="mapka" style="border:none" frameBorder="0"></iframe>
            </div>
				</div>
			</div>
			<div class="row" id="footer">
				<?php include('footer.html'); ?>
			</div>
		</div>
	</div>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
</body>

</html>