<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	include('db.php');
	include('thumbnail.php');

	if(isset($_POST['submit'])){
		if($_POST['password'] != 'Heslo123'){
			return;
		}
		
		$sql2 = "update clanky set nadpis='" . $_POST['nazev'] ."', text='" . $_POST['text_clanku'] ."' where id=" . $_POST['id'];

			if ($conn->query($sql2) === TRUE) {
				echo "<div class=\"alert alert-success\" role=\"alert\">
				Editace proběhla v pořádku
			 </div>";
				header( "refresh:5;url=aktuality.php" );

			} else {
				echo "Error: " . $sql2 . "<br>" . $conn->error;
			}		
		

	}
?>
<?php include('header.html'); ?>
<title>Úvod</title>

<body>
	<div class="container">
		<div class="box-shadow">
			<div class="row">
				<?php include('head-image.php'); ?>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="content">
					<?php
						if(isset($_GET['id'])){
							//nacteni clanku
							$sql = "select id,nadpis,text from clanky where id=" . $_GET["id"];
							$result = mysqli_query($conn, $sql);
							$row = mysqli_fetch_assoc($result);
						
					?>
						<form action="admin-editace-clanku.php" method="POST">
								<input type="text" class="form-control" type="hidden" id="id" name="id" value="<?php echo $row["id"] ?>">
							<div class="form-group">
								<label for="nazev">Nadpis článku</label>
								<input type="text" class="form-control" id="nazev" name="nazev" value="<?php echo $row["nadpis"] ?>" placeholder="Nadpis článku">
							</div>
							<div class="form-group">
								<label for="text_clanku">Text článku</label>
								<textarea class="form-control" id="text_clanku" name="text_clanku" rows="5"><?php echo $row["text"]?></textarea>
							</div>
							<div class="form-group">
								<label for="fotky">Fotky</label>
								<input type="file" class="form-control-file" id="fotky" name="fotky[]" accept="image/*"
									multiple="multiple">
							</div>
							<div class="form-group">
								<input type="password" id="pass" name="password" minlength="5" required>
							</div>
							<button type="submit" name="submit" class="btn btn-primary">Submit</button>
						</form>
						<?php
						}
						?>
					</div>
				</div>
			</div>
			<div class="row" id="footer">
				<?php include('footer.html'); ?>
			</div>
		</div>
	</div>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<script>
	function toggleChevron(e) {
		console.log('AAA');
		$(e.target)
			.prev('.card-header')
			.find("i.fa")
			.toggleClass('fa-chevron-left fa-chevron-down');
	}

	$('#accordion').on('hidden.bs.collapse', toggleChevron);
	$('#accordion').on('shown.bs.collapse', toggleChevron);
	</script>
</body>

</html>