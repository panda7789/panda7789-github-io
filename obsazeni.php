<?php include('header.html'); ?>
<title>Obsazení</title>

<body>
	<div class="container">
		<div class="box-shadow">
			<div class="row">
			<?php include('head-image.php'); ?>
			</div>
			<div class="row" id="menu">
				<?php include('menu.php'); ?>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="content">
						<h3>Obsazení</h3>
						<br>
						<div id="accordion" role="tablist" aria-multiselectable="true">
							<div class="card">
								<h5 class="card-header" role="tab" id="headingOne">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
										aria-controls="collapseOne" class="d-block">
										<i class="fa fa-chevron-down float-right"></i> Zpěvy
									</a>
								</h5>
								<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Soňa Rifflerová</h5>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Markéta Tichá</h5>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Pavel Braun</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<h5 class="card-header" role="tab" id="headingTwo">
									<a class="collapsed d-block" data-toggle="collapse" data-parent="#accordion"
										href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
										<i class="fa fa-chevron-left float-right"></i> Křídlovky
									</a>
								</h5>
								<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Milan Novák</h5>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Pavel Braun</h5>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Tomáš Braun</h5>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Jiří Šimek</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<h5 class="card-header" role="tab" id="headingThree">
									<a class="collapsed d-block" data-toggle="collapse" data-parent="#accordion"
										href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										<i class="fa fa-chevron-left float-right"></i> Tenor
									</a>
								</h5>
								<div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Jaroslava Jašková</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<h5 class="card-header" role="tab" id="headingFour">
									<a class="collapsed d-block" data-toggle="collapse" data-parent="#accordion"
										href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
										<i class="fa fa-chevron-left float-right"></i> Baryton
									</a>
								</h5>
								<div id="collapseFour" class="collapse" role="tabpanel" aria-labelledby="headingFour">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Stanislav Veselý</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<h5 class="card-header" role="tab" id="headingFive">
									<a class="collapsed d-block" data-toggle="collapse" data-parent="#accordion"
										href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
										<i class="fa fa-chevron-left float-right"></i> Klarinety
									</a>
								</h5>
								<div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFive">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Eva Nejedlá</h5>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Zdeněk Nejedlý</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<h5 class="card-header" role="tab" id="headingSix">
									<a class="collapsed d-block" data-toggle="collapse" data-parent="#accordion"
										href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
										<i class="fa fa-chevron-left float-right"></i> Tuba
									</a>
								</h5>
								<div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingSix">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Radoslav Tichý</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<h5 class="card-header" role="tab" id="heading7">
									<a class="collapsed d-block" data-toggle="collapse" data-parent="#accordion"
										href="#collapse7" aria-expanded="false" aria-controls="collapse7">
										<i class="fa fa-chevron-left float-right"></i> Bicí
									</a>
								</h5>
								<div id="collapse7" class="collapse" role="tabpanel" aria-labelledby="heading7">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">František Nejedlý</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="card">
								<h5 class="card-header" role="tab" id="heading8">
									<a class="collapsed d-block" data-toggle="collapse" data-parent="#accordion"
										href="#collapse8" aria-expanded="false" aria-controls="collapse8">
										<i class="fa fa-chevron-left float-right"></i> Doprovodná sekce
									</a>
								</h5>
								<div id="collapse8" class="collapse" role="tabpanel" aria-labelledby="heading8">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Lukáš Linhart</h5>
													</div>
												</div>
											</div>
											<div class="col-sm-3">
												<div class="card ">
													<div class="card-body">
														<h5 class="card-title">Pavel Kozák</h5>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="footer">
			<?php include('footer.html'); ?>
		</div>
	</div>
	</div>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<script>
function toggleChevron(e) {
	console.log('AAA');
  $(e.target)
    .prev('.card-header')
    .find("i.fa")
    .toggleClass('fa-chevron-left fa-chevron-down');
}

$('#accordion').on('hidden.bs.collapse', toggleChevron);
$('#accordion').on('shown.bs.collapse', toggleChevron);
</script>
</body>

</html>