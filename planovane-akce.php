<?php 
include('header.html'); 
include('db.php'); 
SetLocale(LC_ALL, "Czech");
$dny = array('Neděle', 'Pondělí', 'Úterý', 'Středa', 'Čtvrtek', 'Pátek', 'Sobota');

?>
<title>Plánované akce</title>

<body>
	<div class="container">
		<div class="box-shadow">
			<div class="row">
				<?php include('head-image.php'); ?>
			</div>
			<div class="row" id="menu">
				<?php include('menu.php'); ?>
			</div>
			<div class="row">
				<div class="col-sm-8">
					<div class="content">
						<h3 class="display-4">Plánované akce</h3>
						<div class="row">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th scope="col">Datum konání</th>
										<th scope="col">Název akce</th>
										<th scope="col">Místo konání</th>
										<th scope="col">Typ akce</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										$sqlClanek = "SELECT nazev,datum,typ,misto FROM planovane_akce order by datum asc";
							
										$result = $conn->query($sqlClanek);
										if ($result->num_rows > 0) {
											while($row = $result->fetch_assoc()) {
												?>
									<tr>
										<td style="text-align:right"><?php echo $dny[date("w", strtotime($row["datum"]))] . date(" d.m", strtotime($row["datum"])); ?></td>
										<td><?php echo $row["nazev"]; ?></td>
										<td><?php echo $row["misto"]; ?></td>
										<td><?php echo $row["typ"]; ?></td>
									</tr>
									<?php
											}
										} else {
											echo "0 results";
										}
										$conn->close();
									?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="right">
						<iframe
							src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FLo%25C5%25A1tick%25C3%25A1-Veselka-111782970262375&tabs=events&width=340&height=400&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1606107826281502"
							width="340" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
							allowTransparency="true" allow="encrypted-media"></iframe>
					</div>
				</div>
			</div>
			<div class="row" id="footer">
				<?php include('footer.html'); ?>
			</div>
		</div>
	</div>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
</body>

</html>
