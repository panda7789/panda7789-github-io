<?php 
		include('db.php');
		include('thumbnail.php');
?>
<?php include('header.html'); ?>
<title>Úvod</title>

<body>
	<div class="container">
		<div class="box-shadow">
			<div class="row">
				<?php include('head-image.php'); ?>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="content">

						<div class="list-group">
									<?php 
										$sql = "SELECT id,nadpis from clanky";
										$result2 = $conn->query($sql);
										while($row2 = $result2->fetch_assoc()) {
												echo "<a href=\"admin-editace-clanku.php?id=". $row2['id'] . "\" class=\"list-group-item list-group-item-action\">" . $row2['nadpis'] . "</a>";
										}
									?>

						</div>
					</div>
				</div>
			</div>
			<div class="row" id="footer">
				<?php include('footer.html'); ?>
			</div>
		</div>
	</div>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
	<script>
	function toggleChevron(e) {
		console.log('AAA');
		$(e.target)
			.prev('.card-header')
			.find("i.fa")
			.toggleClass('fa-chevron-left fa-chevron-down');
	}

	$('#accordion').on('hidden.bs.collapse', toggleChevron);
	$('#accordion').on('shown.bs.collapse', toggleChevron);
	</script>
</body>

</html>