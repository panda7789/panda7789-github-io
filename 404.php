<?php include('header.html'); ?>
<title>Úvod</title>

<body>
    <div class="container">
        <div class="box-shadow">
            <div class="row">
					<?php include('head-image.php'); ?>
            </div>
            <div class="row" id="menu">
					<?php include('menu.php'); ?>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <div class="content">
                        <h1 class="display-4">OOPS! Chyba 404:</h1>
                        <h2>Požadovaná adresa nefunguje, kontaktujte správce webu.</h2>
                    </div>
                </div>
				</div>
            <div class="row" id="footer">
					<?php include('footer.html'); ?>			
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>
