<?php 
	include('db.php');
	include('header.html');
	include('thumbnail.php'); 
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	if (isset($_GET['page']) && $_GET['page'] >=1) {
		$page = $_GET['page'];
	} else {
		$page = 1;
	}

	$no_of_records_per_page = 12;
	$total_pages_sql = "SELECT COUNT(*) FROM galerie where zobrazit_v_galerii = 1";
	$result = $conn->query($total_pages_sql);
	$total_rows = mysqli_fetch_array($result)[0];
	$total_pages = ceil($total_rows / $no_of_records_per_page);
	if($page > $total_pages){
		$page=1;
	}
	$offset = ($page-1) * $no_of_records_per_page;
?>
<title>Fotogalerie</title>

<body>
    <div class="container">
        <div class="box-shadow">
            <div class="row">
                <?php include('head-image.php'); ?>
            </div>
            <div class="row" id="menu">
                <?php include('menu.php'); ?>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="content">
                        <h3 class="display-4">Fotogalerie</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php 
								$sql = "SELECT url, urlT FROM galerie where zobrazit_v_galerii = 1 LIMIT $offset, $no_of_records_per_page";
								$result2 = $conn->query($sql);
								while($row = mysqli_fetch_array($result2)){
									"<img class=\"image-article\" name=" . $row['url'] . " src=" . $row['url'] . ">";
										echo "<a data-fancybox=\"gallery\" href=" . $row['url'] . "><img class=\"thumb\" src=" . $row['urlT'] ."></a>";
								}
								?>

                            </div>
                        </div>
                        <div class="row justify-content-center">
                            <div class="col-sm-4 ">
                                <nav aria-label="...">
                                    <ul class="pagination">

                                        <li class="page-item <?php if($page==1){echo 'disabled';} ?>">
                                            <a class="page-link" href="<?php echo "?page=".($page - 1); ?>"
                                                tabindex="-1">Předchozí</a>
                                        </li>
                                        <?php for($i=0;$i<$total_pages;$i++){
											?>
                                        <li class="page-item <?php if($page == $i+1){echo "active";} ?>"><a
                                                class="page-link"
                                                href="<?php echo "?page=".($i+1); ?>"><?php echo $i+1 ?></a></li>
                                        <?php 
										} ?>
                                        <li class="page-item <?php if($page==$total_pages){echo 'disabled';} ?>">
                                            <a class="page-link" href="<?php echo "?page=".($page + 1); ?>">Další</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="right">
                <iframe
                    src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FLo%25C5%25A1tick%25C3%25A1-Veselka-111782970262375&tabs=events&width=340&height=400&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=1606107826281502"
                    width="340" height="400" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
                    allowTransparency="true" allow="encrypted-media"></iframe>
            </div>
        </div>
    </div>
    <div class="row" id="footer">
        <?php include('footer.html'); ?>
    </div>
    </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>
