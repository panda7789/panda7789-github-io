<?php 
	include('db.php');
	include('header.html');
	include('thumbnail.php'); 
?>
<title>Aktuality</title>

<body>
	<div class="container">
		<div class="box-shadow">
			<div class="row">
				<?php include('head-image.php'); ?>
			</div>
			<div class="row" id="menu">
				<?php include('menu.php'); ?>
			</div>
			<div class="row">
				<div class="col-sm-8">
					<div class="content">
						<h3 class="display-4">Aktuality</h3>
						<?php 
							$sqlClanek = "SELECT id, nadpis, text FROM clanky order by id desc";
							
							$result = $conn->query($sqlClanek);
							if ($result->num_rows > 0) {
								while($row = $result->fetch_assoc()) {
									?>
						<div class="row">
							<div class="article col-sm-12">
								<div class="article-header">
									<h3>
										<?php echo $row["nadpis"]; ?>
									</h3>
									<hr>
								</div>
								<div class="article-content">
									<p>
										<?php echo $row["text"]; ?>
									</p>
								</div>
								<div class="article-images">
									
										<?php 
													$sqlObrazky = "SELECT url, urlT from galerie where id_clanku = " . $row["id"];
													$result2 = $conn->query($sqlObrazky);
													$i = 0;
													if ($result2->num_rows > 0) {
														while($row2 = $result2->fetch_assoc()) {
															if($i<3){
																if($i==0){
																	?>
																	<div class="first-three-images">
																	<?php
																}
																?>
																<a data-fancybox="gallery" href="<?php echo $row2['url'] ?>">
																	<?php 
																	if(mysqli_num_rows($result2) == 1){
																		?>
																		<img class="thumb-max mx-auto d-block" src="<?php echo $row2['urlT'] ?>">
																		<?php
																	}
																	else{
																		?>
																		<img class="thumb" src="<?php echo $row2['urlT'] ?>">
																		<?php
																	}
																	?>
																</a>
																<?php 
															}
															else{
																if($i==3){
																	?>
																	</div>
																	<div class="collapse" id="collapseExample<?php echo $row["id"]; ?>" aria-expanded="false">
																	<?php
																}
																?>
																<a data-fancybox="gallery" href="<?php echo $row2['url'] ?>">
																<img class="thumb" src="<?php echo $row2['urlT'] ?>">
																</a>
																<?php
															}
															$i+=1;
														}?>
														</div>
														<?php 
														if(mysqli_num_rows($result2) > 3){
															?>
															<a role="button" class="collapsed" id="collapseButton" data-toggle="collapse"
															href="#collapseExample<?php echo $row['id']; ?>" aria-expanded="false" aria-controls="collapseExample<?php echo $row['id']; ?>"></a>
															<?php 
														}
													}
													?>
								</div>

							</div>
						</div>
						<?php
								}
							} else {
								echo "0 results";
							}
							$conn->close();
						?>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="right">
					<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FLo%25C5%25A1tick%25C3%25A1-Veselka-111782970262375%2F&tabs=timeline%2Cevents&width=340&height=900&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=false&appId=1606107826281502" style="border:none;overflow:hidden;" id="facebook-aktuality" height="1000px" width="340" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
					</div>
				</div>
			</div>
			<div class="row" id="footer">
				<?php include('footer.html'); ?>
			</div>
		</div>
	</div>


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
	</script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
	</script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
	</script>
</body>

</html>